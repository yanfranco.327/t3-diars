﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace T3_Diars.Models
{
    public class Rutina
    {
        public int Id { get; set; }


        [Required(ErrorMessage = "Este campo es obligatorio")]
        public string Nombre { get; set; }

        [Required(ErrorMessage = "Este campo es obligatorio")]
        public string Tipo { get; set; }
        public int UsuarioId { get; set; }

        public List<EjercicioRutina> Ejercicios { get; set; }

    }
}
