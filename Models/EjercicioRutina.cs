﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace T3_Diars.Models
{
    public class EjercicioRutina
    {
        public int Id { get; set; }

        public int RutinaId { get; set; }
        public int EjercicioId { get; set; }
        public int Tiempo { get; set; }
        public Ejercicio Ejercicio { get; set; }

    }
}
