﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using T3_Diars.DB;
using T3_Diars.Estrategia;
using T3_Diars.Models;

using Microsoft.EntityFrameworkCore;

namespace T3_Diars.Controllers
{
    public class RutinaController : Controller
    {
        private AppT3Context context;
        private Tipo tipo;
        public RutinaController(AppT3Context context)
        {
            this.context = context;
        }
        public ActionResult Index()
        {
            var usuario = GetUsuario();
            var rutinas = context.Rutinas.Where(o => o.UsuarioId == usuario.Id);
            return View(rutinas);
        }

        public ActionResult Ejercicios(int id)
        {
            var usuario = GetUsuario();
            var ejercicioRutinas = context.EjercicioRutinas.Where(o => o.RutinaId == id).Include(o => o.Ejercicio).ToList(); 

            return View(ejercicioRutinas);
        }

        [HttpGet]
        public ActionResult Create()
        {
            ViewBag.usuario = GetUsuario();
            return View(new Rutina());
        }

        [HttpPost]
        public ActionResult Create(Rutina rutina)
        {
            if (ModelState.IsValid)
            {
                switch (rutina.Tipo) {
                    case "Principiante":
                        tipo = new Principiante();
                        break;
                    case "Intermedio":
                        tipo = new Intermedio();
                        break;
                    case "Avanzado":
                        tipo = new Avanzado();
                        break;
                }
              var ejerciciosSeleccionados = tipo.listaEjercicios(context);

                context.Rutinas.Add(rutina);
                context.SaveChanges();

                var rutinas = context.Rutinas.ToList();
                var ultimaRutina = rutinas.Last();

                Random random = new Random();
                int tiempo = 120;
                foreach (var ejercicio in ejerciciosSeleccionados)
                {
                    if (ultimaRutina.Tipo != "Avanzado")
                    {
                        tiempo = random.Next(60, 120);
                    }
                    context.EjercicioRutinas.Add(new EjercicioRutina() { RutinaId = ultimaRutina.Id, EjercicioId = ejercicio.Id, Tiempo = tiempo });
                    context.SaveChanges();
                }


                return RedirectToAction("Index");
            }

            return View("Create", rutina);
        }
        private Usuario GetUsuario()
        {
            var claim = HttpContext.User.Claims.FirstOrDefault();
            if (claim != null)
            {
                var user = context.Usuarios.Where(o => o.Username == claim.Value).FirstOrDefault();
                return user;
            }

            return null;
        }

    }
}
