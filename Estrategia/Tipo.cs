﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using T3_Diars.DB;
using T3_Diars.Models;

namespace T3_Diars.Estrategia
{
    public abstract class Tipo
    {
        
        public abstract List<Ejercicio> listaEjercicios(AppT3Context context);
        
    }
}
