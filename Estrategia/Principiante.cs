﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using T3_Diars.DB;
using T3_Diars.Models;

namespace T3_Diars.Estrategia
{
    public class Principiante : Tipo
    {
       
        public override List<Ejercicio> listaEjercicios(AppT3Context context)
        {
            var ejercicios = context.Ejercicios.OrderBy(or => Guid.NewGuid()).Take(5).ToList();
            return ejercicios;
        }
    }
}
