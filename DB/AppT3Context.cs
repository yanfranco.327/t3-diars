﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using T3_Diars.DB.Mapping;
using T3_Diars.Models;

namespace T3_Diars.DB
{
    public class AppT3Context : DbContext
    {
        public DbSet<Usuario> Usuarios { get; set; }
        public DbSet<Ejercicio> Ejercicios { get; set; }
        public DbSet<Rutina> Rutinas{ get; set; }
        public DbSet<EjercicioRutina> EjercicioRutinas{ get; set; }

        public AppT3Context(DbContextOptions<AppT3Context> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.ApplyConfiguration(new UsuarioMap());
            modelBuilder.ApplyConfiguration(new EjercicioMap());
            modelBuilder.ApplyConfiguration(new EjercicioRutinaMap());
            modelBuilder.ApplyConfiguration(new RutinaMap());


        }
    }
}
