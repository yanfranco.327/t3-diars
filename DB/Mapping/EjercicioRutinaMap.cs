﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using T3_Diars.Models;

namespace T3_Diars.DB.Mapping
{
    public class EjercicioRutinaMap : IEntityTypeConfiguration<EjercicioRutina>
    {
        public void Configure(EntityTypeBuilder<EjercicioRutina> builder)
        {
            builder.ToTable("EjercicioRutina");
            builder.HasKey(o => o.Id);
            builder.HasOne(o => o.Ejercicio).
                WithMany().
                HasForeignKey(o => o.EjercicioId);

        }
    }
}
